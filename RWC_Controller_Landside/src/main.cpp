#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Empty.h>

class ControllerLandside
{
  public:

  ControllerLandside()
  {
    // Set up publisher and subscriber
    _pub = _nh.advertise<sensor_msgs::Joy>( "/rwc/ctl/controls", 1000 );
    _errPub = _nh.advertise<std_msgs::Empty>( "/rwc/ctl/fault", 1000 );
    _resolvedPub = _nh.advertise<std_msgs::Empty>( "/rwc/ctl/resolved", 1000 );
    _sub = _nh.subscribe<sensor_msgs::Joy>(
      "joy",
      1000,
      &ControllerLandside::callback,
      this);
  }

  void callback( const sensor_msgs::Joy::ConstPtr& msg )
  {
    _currentCtl.header = msg->header;

    // TODO: Just put the required axes and buttons in the curren controls
    _currentCtl.axes = msg->axes;
    _currentCtl.buttons = msg->buttons;


    // Save time of this message
    _timeLastMsg = ros::Time::now();

    // Set initialization to done
    if ( !_initialized )
      _initialized = true;
  }

  void sendControls()
  {
    if ( !_initialized )
      return;

    // Check if new control msgs are still coming in
    ros::Time currentTime = ros::Time::now();
    ros::Duration timeSinceLastMsg = currentTime - _timeLastMsg;
    if ( timeSinceLastMsg.toSec() > 0.25 )
    {
      // More than 0.25 secs no new msg leads to an error being sent
      ROS_ERROR(
        "No new Ctl-Msg since %f seconds!",
        timeSinceLastMsg.toSec()
      );

      // Only sent the error once
      if ( !_fault )
        _errPub.publish( std_msgs::Empty() );

      _fault = true;
    }
    else if ( _fault == true )
    {
      // After the fault got resolved a resolved msg will be sent
      ROS_INFO( "Error got resolved" );

      _fault = false;
      _resolvedPub.publish( std_msgs::Empty() );
    }

    // Always send the last known controller configuration
    _pub.publish( _currentCtl );
  }

  // ---------- Getters and Setters ---------
  ros::NodeHandle* getNodeHandle() { return &_nh; }


  private:

  // ROS variables
  ros::NodeHandle _nh;
  ros::Publisher _pub;
  ros::Publisher _errPub;
  ros::Publisher _resolvedPub;
  ros::Subscriber _sub;

  // Ctl variables
  ros::Time _timeLastMsg = ros::Time::now();
  sensor_msgs::Joy _currentCtl;
  bool _initialized = false;

  // Fault variables
  bool _fault = false;

};

int main( int argc, char **argv )
{
  ROS_INFO( "STARTING CONTROLLER LANDSIDE" );

  ros::init( argc, argv, "controller_landside" );
  ControllerLandside controllerLandside;
  ros::NodeHandle* nh = controllerLandside.getNodeHandle();
  ros::Rate rate(60.); // Controller sending Msgs at 60Hz

  ROS_INFO( "STARTED" );

  while ( nh->ok() )
  {
    controllerLandside.sendControls();

    rate.sleep();
    ros::spinOnce();
  }

  ROS_INFO( "SHUTTING DOWN" );

  return 0;
}
